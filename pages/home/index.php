<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>THE HAY | Home</title>
      <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];

        $USERIMG = isset($_SESSION['member'][0]['user_img'])?$_SESSION['member'][0]['user_img']:"";

      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <style>
      th {
        text-align:center;
      }
    </style>
    <body class="hold-transition skin-blue sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Welcome
            </h1>
          </section>

          <!-- Main content -->
          <section class="content">
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-7">
                <div class="box box-solid" style="background-color: #13436b;">
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-2" align ="center">
                          <img src="<?= $USERIMG ?>" style="width:120px;" onerror="this.onerror='';this.src='images/user.png'" class="img-circle" alt="User Image">
                        </div>
                        <div class="col-md-9 use-name">
                          <div class="widget-user-username" style="font-size: 24px;">ชื่อ : Nadia Carmichael</div>
                          <div class="widget-user-desc" style="font-size: 18px;">ตำแหน่ง : Developer</div>
                          <div class="widget-user-desc" style="font-size: 18px;">ID : IX001</div>
                        </div>
						<div class="col-md-1 label-status">
							<span class="label label-success">Active</span>
						</div>
                      </div>
                    </div>
                </div>
                <div class="box box-solid">
                    <div class="box-header">
                      <h3 class="box-title">Lastest Login</h3>
                    </div>
                    <div class="box-body no-padding">
                      <table class="table table-striped">
                        <tbody><tr>
                          <th>ID</th>
                          <th>Date</th>
                          <th>Time</th>
                        </tr>
                        <tr>
                          <td>1234</td>
                          <td>23/03/2019</td>
                          <td>09.30</td>
                        </tr>
                        <tr>
                          <td>1234</td>
                          <td>23/03/2019</td>
                          <td>09.30</td>
                        </tr>
                        <tr>
                          <td>1234</td>
                          <td>23/03/2019</td>
                          <td>09.30</td>
                        </tr>
                        <tr>
                          <td>1234</td>
                          <td>23/03/2019</td>
                          <td>09.30</td>
                        </tr>
                      </tbody></table>
                    </div>
                  </div>
                  <div class="row">
                    <div style="padding:5px;">
                      <a class="btn btn-app bg-Hay">
                        <i class="fa fa-dashboard"></i> Dashboard
                      </a>
                      <a class="btn btn-app bg-Hay">
                        <i class="fa fa-star-o"></i> Sales
                      </a>
                      <a class="btn btn-app bg-Hay">
                        <i class="fa fa-briefcase"></i> Marketing
                      </a>
                      <a class="btn btn-app bg-Hay">
                        <i class="fa fa-industry"></i> Invertory
                      </a>
                      <a class="btn btn-app bg-Hay">
                        <i class="fa fa-sliders"></i> Management
                      </a>
                    </div>
                  </div>
              </div>
              <div class="col-md-5">
                <div class="box box-solid" style="background:#13436b;color:#ffffff; padding: 10px 10px 30px; 10px; font-size: 16px;">
                  <div class="box-header ui-sortable-handle" style="cursor: move;color:#ffffff">
                    <i class="fa fa-calendar"></i>
                    <h3 class="box-title">Calendar</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding" style="text-align:center;">
                    <!--The calendar -->
                    <div id="calendar" style="width: 100%;">
                      <div class="datepicker datepicker-inline">
                        <div class="datepicker-days" style="">
                          <table class="table-condensed">
                            <thead>
                              <tr>
                                <th colspan="7" class="datepicker-title" style="display: none;"></th>
                              </tr>
                              <tr>
                                <th class="prev">«</th>
                                <th colspan="5" class="datepicker-switch">July 2022</th>
                                <th class="next">»</th>
                              </tr>
                              <tr>
                                <th class="dow">Su</th><th class="dow">Mo</th><th class="dow">Tu</th><th class="dow">We</th><th class="dow">Th</th><th class="dow">Fr</th><th class="dow">Sa</th></tr></thead><tbody><tr><td class="old day" data-date="1656201600000">26</td><td class="old day" data-date="1656288000000">27</td><td class="old day" data-date="1656374400000">28</td><td class="old day" data-date="1656460800000">29</td><td class="old day" data-date="1656547200000">30</td><td class="day" data-date="1656633600000">1</td><td class="day" data-date="1656720000000">2</td></tr><tr><td class="day" data-date="1656806400000">3</td><td class="day" data-date="1656892800000">4</td><td class="day" data-date="1656979200000">5</td><td class="day" data-date="1657065600000">6</td><td class="day" data-date="1657152000000">7</td><td class="day" data-date="1657238400000">8</td><td class="day" data-date="1657324800000">9</td></tr><tr><td class="day" data-date="1657411200000">10</td><td class="day" data-date="1657497600000">11</td><td class="day" data-date="1657584000000">12</td><td class="day" data-date="1657670400000">13</td><td class="day" data-date="1657756800000">14</td><td class="day" data-date="1657843200000">15</td><td class="day" data-date="1657929600000">16</td></tr><tr><td class="day" data-date="1658016000000">17</td><td class="day" data-date="1658102400000">18</td><td class="day" data-date="1658188800000">19</td><td class="day" data-date="1658275200000">20</td><td class="day" data-date="1658361600000">21</td><td class="day" data-date="1658448000000">22</td><td class="day" data-date="1658534400000">23</td></tr><tr><td class="day" data-date="1658620800000">24</td><td class="day" data-date="1658707200000">25</td><td class="day" data-date="1658793600000">26</td><td class="day" data-date="1658880000000">27</td><td class="day" data-date="1658966400000">28</td><td class="day" data-date="1659052800000">29</td><td class="day" data-date="1659139200000">30</td></tr><tr><td class="day" data-date="1659225600000">31</td><td class="new day" data-date="1659312000000">1</td><td class="new day" data-date="1659398400000">2</td><td class="new day" data-date="1659484800000">3</td><td class="new day" data-date="1659571200000">4</td><td class="new day" data-date="1659657600000">5</td><td class="new day" data-date="1659744000000">6</td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th colspan="7" class="datepicker-title" style="display: none;"></th></tr><tr><th class="prev">«</th><th colspan="5" class="datepicker-switch">2022</th><th class="next">»</th></tr></thead><tbody><tr><td colspan="7"><span class="month">Jan</span><span class="month">Feb</span><span class="month">Mar</span><span class="month">Apr</span><span class="month">May</span><span class="month">Jun</span><span class="month focused">Jul</span><span class="month">Aug</span><span class="month">Sep</span><span class="month">Oct</span><span class="month">Nov</span><span class="month">Dec</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th colspan="7" class="datepicker-title" style="display: none;"></th></tr><tr><th class="prev">«</th><th colspan="5" class="datepicker-switch">2020-2029</th><th class="next">»</th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2019</span><span class="year">2020</span><span class="year">2021</span><span class="year focused">2022</span><span class="year">2023</span><span class="year">2024</span><span class="year">2025</span><span class="year">2026</span><span class="year">2027</span><span class="year">2028</span><span class="year">2029</span><span class="year new">2030</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-decades" style="display: none;"><table class="table-condensed"><thead><tr><th colspan="7" class="datepicker-title" style="display: none;"></th></tr><tr><th class="prev">«</th><th colspan="5" class="datepicker-switch">2000-2090</th><th class="next">»</th></tr></thead><tbody><tr><td colspan="7"><span class="decade old">1990</span><span class="decade">2000</span><span class="decade">2010</span><span class="decade focused">2020</span><span class="decade">2030</span><span class="decade">2040</span><span class="decade">2050</span><span class="decade">2060</span><span class="decade">2070</span><span class="decade">2080</span><span class="decade">2090</span><span class="decade new">2100</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div><div class="datepicker-centuries" style="display: none;"><table class="table-condensed"><thead><tr><th colspan="7" class="datepicker-title" style="display: none;"></th></tr><tr><th class="prev">«</th><th colspan="5" class="datepicker-switch">2000-2900</th><th class="next">»</th></tr></thead><tbody><tr><td colspan="7"><span class="century old">1900</span><span class="century focused">2000</span><span class="century">2100</span><span class="century">2200</span><span class="century">2300</span><span class="century">2400</span><span class="century">2500</span><span class="century">2600</span><span class="century">2700</span><span class="century">2800</span><span class="century">2900</span><span class="century new">3000</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today" style="display: none;">Today</th></tr><tr><th colspan="7" class="clear" style="display: none;">Clear</th></tr></tfoot></table></div></div></div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <div class="box box-solid">
                    <div class="box-header">
                      <h3 class="box-title">Lastest Orders</h3>
                    </div>
                    <div class="box-body no-padding">
                      <table class="table table-striped">
                        <tbody><tr>
                          <th>Order ID</th>
                          <th>item</th>
                          <th>status</th>
                        </tr>
                        <tr>
                          <td>OR8999</td>
                          <td>OR8999</td>
                          <td align="center"><span class="label label-success">Success</span></td>
                        </tr>
                        <tr>
                          <td>OR8999</td>
                          <td>OR8999</td>
                          <td align="center"><span class="label label-success">Success</span></td>
                        </tr>
                        <tr>
                          <td>OR8999</td>
                          <td>OR8999</td>
                          <td align="center"><span class="label label-warning">Pending</span></td>
                        </tr>
                        <tr>
                          <td>OR8999</td>
                          <td>OR8999</td>
                          <td align="center"><span class="label label-success">Success</span></td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                    <div class="box-footer clearfix no-border">
                      <button type="button" class="btn btn-flat btn-Hay pull-left">New Order</button>
                      <button type="button" class="btn btn-flat btn-default pull-right">View All</button>
                    </div>
                  </div>
              </div>

            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script src="../../dist/js/bootstrap-datepicker.min.js"></script>
    </body>
  </html>
