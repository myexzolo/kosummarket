<!-- Info boxes -->
<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <img src="../../image/stp1.jpg" style="width:90px; float:left;">

      <div class="info-box-content">
        <span class="info-box-text">1.ข้อมูลลุกค้า</span>
        <span class="info-box-number"><small>สร้างลูกค้าหรือเลือกจากฐานข้อมูล</small></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <img src="../../image/stp2.jpg" style="width:90px; float:left;"> 

      <div class="info-box-content">
        <span class="info-box-text">2.บันทึกใบจอง</span>
        <span class="info-box-number"><small>สร้างเอกสารบันทึกใบจองสนาม</small></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <!-- fix for small devices only -->
  <div class="clearfix visible-sm-block"></div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <img src="../../image/stp3.jpg" style="width:90px; float:left;">

      <div class="info-box-content">
        <span class="info-box-text">3.ยืนยันการจอง</span>
        <span class="info-box-number"><small>Comfirm การจองเข้าใช้งานสนาม</small></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <img src="../../image/stp4.jpg" style="width:90px; float:left;">

      <div class="info-box-content">
        <span class="info-box-text">4.ส่ง Email</span>
        <span class="info-box-number"><small>ส่ง Email รายการจองถึงลูกค้า</small></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
