$( document ).ajaxStart(function() {
  $(".loadingImg").removeClass('none');
});

$( document ).ajaxStop(function() {
  $(".loadingImg").addClass('none');
});

function showProcessbar(){
  // $.smkProgressBar({
  //   element:'body',
  //   status:'start',
  //   bgColor: '#000',
  //   barColor: '#fff',
  //   content: 'Loading...'
  // });
  // setTimeout(function(){
  //   $.smkProgressBar({
  //     status:'end'
  //   });
  // }, 1000);
}

function GenPass(type=0,num=8) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  if(type == 0){
    text = 'P@ss1234';
  }else{
    for (var i = 0; i < num; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
  }
  return text;
}

function showSlidebar(){
  $.get( "../../inc/function/slidebar.php" )
  .done(function( data ) {
    $("#showSlidebar").html(data);
  });
}

function readURL(input,values) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img width='100'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}
