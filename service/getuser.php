<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$typeSearch = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";

$con = "";

$dateNow = date("Y-m-d");


if($typeSearch == "TODAY"){
  $con .= " and DATE_FORMAT(PERSON_REGISTER_DATE,'%Y-%m-%d') = '$dateNow'";
}else if($typeSearch == "ACTIVE"){
  $con .= " and PERSON_STATUS = 'Y'";
}else if($typeSearch == "EXPIRE"){
  $con .= " and PERSON_STATUS <> 'Y'";
}

$sql = "SELECT * FROM person where COMPANY_CODE ='$companycode' $con";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
