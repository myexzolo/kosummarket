<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$dataGroup    = isset($_GET['dataGroup'])?$_GET['dataGroup']:"";

$con = "";

if($dataGroup != ""){
  $con = " and DATA_GROUP = '$dataGroup' ";
}


$sql = "SELECT * FROM data_master where DATA_SHOW = 'Y' $con  ORDER BY DATA_GROUP,DATA_SEQ ";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
